package ru.zolotukhin.counterstudents;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class StartActivity extends Activity {
    private Integer counterStudents=0;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }



    public void onClickBtnAddStudents(View view){
        counterStudents++;
        Button btnOtch=(Button) findViewById(R.id.btn_Otch);
        btnOtch.setEnabled(true);
        TextView counterView=(TextView) findViewById(R.id.txt_students);
        counterView.setText(counterStudents.toString());
    }
    public void onClickBtnOtchStudents(View view){
        counterStudents--;
        if(counterStudents==0){
            view.setEnabled(false);}
        TextView counterView=(TextView) findViewById(R.id.txt_students);
        counterView.setText(counterStudents.toString());
    }
}
